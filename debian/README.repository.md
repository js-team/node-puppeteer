Notes about this repository layout
==================================

I have prepared this package using a different layout than other pkg-js
packages, please try to maintain this style if you modify/update the pacakge.

Devtools-protocol package
-------------------------

Since puppeteer has a tight dependency on this package, I have decided to embed
it as a "component" with a fixed version instead of dependending on a separate
Debian package.

Upstream history
----------------

The upstream git repository is fully cloned, which brings many advantages,
specially when debugging issues. But instead of keeping `dfsg` branches for
manual repackaging, `gbp import-orig` is used with extra flags to automatically
download and repackage the original tarball, ensure the upstream branch is
identical to it, while at the same time making this snapshot a descendant of
the upstream release tag.

Updating to a new upstream release
----------------------------------

These steps assume you have a git remote configured for upstream, if that is
not the case, run:

    $ git remote add upstream https://github.com/puppeteer/puppeteer.git

Steps to update to (fictitious) version 99.1.1:


1. Fetch upstream history.

    $ git fetch --tags upstream

2. Check the required Chromium version, and abort the process if that version
is not yet available in unstable:

    $ NEW_VER=99.1.1
    $ git show v$NEW_VER:versions.js

3. Check the required devtools-protocol version in the prospective release
(uses `jq` for parsing JSON) and update `debian/watch`.

    $ DEVTOOLS_VER=$(git show v$NEW_VER:package.json | \
        jq --raw-output '.dependencies["devtools-protocol"]')
    $ sed -i "s#\(/devtools-protocol-\)(0.0.[0-9]*)#\1($DEVTOOLS_VER)#" \
        debian/watch

4. Commit the change and use `gbp import-orig` to download the upstream
tarballs, repack, and do the magic merge.

    $ gbp import-orig --upstream-vcs-tag=v$NEW_VER -u $NEW_VER+dfsg --uscan
    $ dch -v$NEW_VER+dfsg-1 'New upstream release.'

5. Now continue with normal packaging adjustments. Don't forget to update the
chromium dependency!

Branches
--------

Follows loosely DEP-14:

* debian/<distribution>
  debian/sid (default branch)

  Packaging branches for Debian.
  Keeping separate branches per distribution eases backporting and feature
  branches, without polluting the namespace.

* upstream/latest

  Snapshot of upstream tree, identical to the (repackaged) tarball. This branch
  only contains merge commits that join upstream history.
